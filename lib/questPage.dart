
import 'dart:math';

import 'package:app_game/scorePage.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

import 'reviewQuest.dart';

class QuestionPage extends StatefulWidget {
  String idQuestion;
  String numStar;
  String score;
  QuestionPage(
      {Key? key,
      required this.idQuestion,
      required this.numStar,
      required this.score})
      : super(key: key);

  @override
  _QuestionPageState createState() =>
      _QuestionPageState(this.idQuestion, this.numStar, this.score);
}

class _QuestionPageState extends State<QuestionPage> {
  String idQuestion;
  String numStar;
  String score;
  String question = "";
  String answer = "";
  String fake = "";
  var quest;
  int randomNumber = Random().nextInt(2);
  int printAnswer = 9;
  bool status = true;
  bool flag = false;

  _QuestionPageState(this.idQuestion, this.numStar, this.score);

  @override
  void initState() {
    super.initState();
    ReviewQuestions().getQuestions().then((QuerySnapshot querySnapshot) {
      if (querySnapshot.docs.isNotEmpty) {
        if (querySnapshot.size >= int.parse(idQuestion)) {
          querySnapshot.docs.forEach((data) {
            if (data["id"].toString() == idQuestion) {
              print(data["id"]);
              // print(data["questions"]);
              // print(data["answer"]);
              // print(data["fake"]);
              setState(() {
                question = data["questions"].toString();
                answer = data["answer"].toString();
                fake = data["fake"].toString();
              });
            }
          });
        } else {
          print("out of data.");
          flag = true;
        }
      } else {
        print("data is empty.");
      }
    });
  }

  String answerchoice() {
    String showAnswer =
        (randomNumber % 2 == 0) ? answer.toString() : fake.toString();
    return showAnswer;
  }

  int resultAnswer(int val) {
    int showResultAnswer = (val == randomNumber) ? 0 : 1;
    return showResultAnswer;
  }

  String returnAnswer(int val) {
    numStar = (int.parse(numStar) - val).toString();
    String showResultAnswer = (val == 0) ? "ถูกต้อง" : "ผิด";
    return showResultAnswer;
  }

  String increaseId(idQuestion) {
    int temp = int.parse(idQuestion) + 1;
    String newIdQuest = temp.toString();
    return newIdQuest;
  }

  String increaseScore() {
    int temp = int.parse(score) + 1;
    String newScore = temp.toString();
    return newScore;
  }

  colorFuntion(int val) {
    if (val == 0) {
      setState(() {
        score = (int.parse(score) + 1).toString();
      });
      return Colors.green;
    } else if (val == 1) {
      if (int.parse(numStar) > 1) {
        numStar = (int.parse(numStar) - 1).toString();
      } else {
        numStar = "0";
        print("xxxx");
        flag = true;
      }

      return Colors.red;
    } else {
      return Colors.yellow;
    }
  }

  checkFuntion(int val) {
    setState(() {
      if (val == 0) {
        printAnswer = resultAnswer(0);
      } else if (val == 1) {
        printAnswer = resultAnswer(1);
      } else {}
    });
  }

  disableButton() {
    setState(() {
      status = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.blue[700],
        appBar: AppBar(
          title: Text("Question $idQuestion"),
        ),
        body: Container(
          child: Column(
            children: [
              Container(
                //HP
                padding: EdgeInsets.all(16.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    for (int i = 0; i < int.parse(numStar); i++)
                      Icon(
                        Icons.star_border,
                        size: 45,
                        color: Colors.yellow,
                      ),
                  ],
                ),
              ),
              Container(
                //Question
                padding: EdgeInsets.all(8),
                color: Colors.white,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      "$question",
                      style: TextStyle(fontSize: 30),
                    ),
                    Padding(padding: EdgeInsets.all(8)),
                    Text(
                      answerchoice(),
                      style: TextStyle(fontSize: 25),
                    ),
                    Padding(padding: EdgeInsets.all(56)),
                    Text(
                      status ? "" : answer.toString(),
                      style: TextStyle(fontSize: 25, color: Colors.red),
                    ),
                  ],
                ),
              ),
              Padding(padding: EdgeInsets.all(8)),
              Container(
                  child: Column(
                children: [
                  Text(
                    "Score",
                    style: TextStyle(fontSize: 25, color: Colors.white),
                  ),
                  status
                      ? Text(
                          score,
                          style: TextStyle(fontSize: 25, color: Colors.white),
                        )
                      : Text(
                          increaseScore(),
                          style: TextStyle(fontSize: 25, color: Colors.white),
                        ),
                ],
              )),
              Padding(padding: EdgeInsets.all(8)),
              Container(
                //answer
                color: colorFuntion(printAnswer),
                padding: EdgeInsets.all(64),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  mainAxisSize: MainAxisSize.max,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    TextButton(
                      style: ButtonStyle(
                          shape: MaterialStateProperty.all(CircleBorder()),
                          side: MaterialStateProperty.all(
                              BorderSide(width: 2, color: Colors.green)),
                          foregroundColor:
                              MaterialStateProperty.all<Color>(Colors.green),
                          padding: MaterialStateProperty.all(
                              EdgeInsets.symmetric(
                                  vertical: 10, horizontal: 10)),
                          textStyle: MaterialStateProperty.all(
                              TextStyle(fontSize: 100)),
                          backgroundColor:
                              MaterialStateProperty.all<Color>(Colors.white)),
                      onPressed: status
                          ? () {
                              setState(() {
                                checkFuntion(0);
                                disableButton();
                              });
                            }
                          : null,
                      child: Text(' ใช่ '),
                    ),
                    Padding(padding: EdgeInsets.all(16.0)),
                    TextButton(
                      style: ButtonStyle(
                          shape: MaterialStateProperty.all(CircleBorder()),
                          side: MaterialStateProperty.all(
                              BorderSide(width: 2, color: Colors.red)),
                          foregroundColor:
                              MaterialStateProperty.all<Color>(Colors.red),
                          padding: MaterialStateProperty.all(
                              EdgeInsets.symmetric(
                                  vertical: 10, horizontal: 10)),
                          textStyle: MaterialStateProperty.all(
                              TextStyle(fontSize: 100)),
                          backgroundColor:
                              MaterialStateProperty.all<Color>(Colors.white)),
                      onPressed: status
                          ? () {
                              setState(() {
                                checkFuntion(1);
                                disableButton();
                              });
                            }
                          : null,
                      child: Text(' ไม่ '),
                    ),
                  ],
                ),
              ),
              Container(
                  //Next button
                  padding: EdgeInsets.all(32.0),
                  child: Center(
                      child: TextButton(
                    style: ButtonStyle(
                        side: MaterialStateProperty.all(
                            BorderSide(width: 2, color: Colors.blue)),
                        foregroundColor:
                            MaterialStateProperty.all<Color>(Colors.blue),
                        padding: MaterialStateProperty.all(
                            EdgeInsets.symmetric(vertical: 10, horizontal: 50)),
                        textStyle:
                            MaterialStateProperty.all(TextStyle(fontSize: 30)),
                        backgroundColor:
                            MaterialStateProperty.all<Color>(Colors.white)),
                    onPressed: status == false
                        ? () {
                            setState(() {
                              flag
                                  ? Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => scorePage(
                                                score: this.score,
                                              )))
                                  : Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => QuestionPage(
                                                idQuestion:
                                                    increaseId(idQuestion),
                                                numStar: this.numStar,
                                                score: this.score,
                                              )));
                            });
                          }
                        : null,
                    child: Text('ข้อต่อไป'),
                  ))),
            ],
          ),
        ));
  }
}



import 'package:app_game/questPage.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class App extends StatefulWidget {
  App({Key? key}) : super(key: key);

  @override
  _AppState createState() => _AppState();
}

class _AppState extends State<App> {
  final Future<FirebaseApp> _initalization = Firebase.initializeApp();
  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: _initalization,
        builder: (context, snapshot) {
          if (snapshot.hasError) {
            return Text("Error..");
          } else if (snapshot.connectionState == ConnectionState.done) {
            return MyApp();
          }
          return Text("Loading..");
        });
  }
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Questions YES or NO',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Questions YES or NO'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.blue[700],
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Column(
        children: [
          Container(
              padding: EdgeInsets.all(64),
              color: Colors.white,
              child: Column(
                children: [
                  Text("คำถาม",
                      style: TextStyle(fontSize: 40, color: Colors.black)),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(" ใช่ ",
                          style: TextStyle(fontSize: 50, color: Colors.green)),
                      Text("หรือ",
                          style: TextStyle(fontSize: 30, color: Colors.black)),
                      Text(" ไม่ ",
                          style: TextStyle(fontSize: 50, color: Colors.red)),
                    ],
                  ),
                ],
              )),
          Container(
            padding: EdgeInsets.all(16.0),
            child: Center(
                child: TextButton(
              style: ButtonStyle(
                  side: MaterialStateProperty.all(
                      BorderSide(width: 2, color: Colors.blue)),
                  foregroundColor:
                      MaterialStateProperty.all<Color>(Colors.blue),
                  padding: MaterialStateProperty.all(
                      EdgeInsets.symmetric(vertical: 10, horizontal: 50)),
                  textStyle: MaterialStateProperty.all(TextStyle(fontSize: 30)),
                  backgroundColor:
                      MaterialStateProperty.all<Color>(Colors.white)),
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => QuestionPage(
                            idQuestion: "1", numStar: "3", score: "0")));
              },
              child: Text('เริ่มเล่น'),
            )),
          ),
        ],
      ),
    );
  }
}

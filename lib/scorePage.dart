
import 'package:flutter/material.dart';

class scorePage extends StatefulWidget {
  String score;
  scorePage({Key? key, required this.score}) : super(key: key);

  @override
  _scorePageState createState() => _scorePageState(this.score);
}

class _scorePageState extends State<scorePage> {
  String score;

  _scorePageState(this.score);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.blue[700],
      appBar: AppBar(
        title: Text("score"),
      ),
      body: Container(
          child: Center(
        child: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
          Text("GAME OVER",
              style: TextStyle(fontSize: 50, color: Colors.white)),
          Padding(padding: EdgeInsets.all(16)),
          Text("Your score",
              style: TextStyle(fontSize: 40, color: Colors.white)),
          Padding(padding: EdgeInsets.all(16)),
          Text(
            score,
            style: TextStyle(fontSize: 40, color: Colors.white),
          ),
        ]),
      )),
    );
  }
}
